package application;

// Import statements
// The entirety of the javafx library is included
// as well as Random for a random number generator
import java.util.Random;
import javafx.animation.*;
import javafx.application.*;
import javafx.beans.*;
import javafx.beans.binding.*;
import javafx.beans.property.*;
import javafx.beans.property.adapter.*;
import javafx.beans.value.*;
import javafx.collections.*;
import javafx.collections.transformation.*;
import javafx.concurrent.*;
import javafx.css.*;
import javafx.embed.*;
import javafx.embed.swing.*;
import javafx.event.*;
import javafx.fxml.*;
import javafx.geometry.*;
import javafx.print.*;
import javafx.scene.*;
import javafx.scene.canvas.*;
import javafx.scene.chart.*;
import javafx.scene.control.*;
import javafx.scene.control.cell.*;
import javafx.scene.effect.*;
import javafx.scene.image.*;
import javafx.scene.input.*;
import javafx.scene.layout.*;
import javafx.scene.media.*;
import javafx.scene.paint.*;
import javafx.scene.shape.*;
import javafx.scene.text.*;
import javafx.scene.transform.*;
import javafx.scene.web.*;
import javafx.stage.*;
import javafx.util.*;
import javafx.util.converter.*;

public class Main extends Application {

	Timeline timeline;
	@Override
	public void start(Stage primaryStage) {
		try {
			Group root = new Group();
			Scene scene = new Scene(root,1000,900);
			//TranslateTransition translateTransition;
			// Array list of capacity 10, that contains rectangle objects
			//ArrayList<Rectangle> rectangles = new ArrayList<Rectangle>(10);
			Rectangle[] rectangles = new Rectangle[10];
			Rectangle[] rectangles2 = new Rectangle[10];
			// Variable to store a random integer we will be generating
			// That random integer will be one of the values we will
			// be sorting.  It will be represented by a rectangle
			// whose height is the value of (nextHeight * 100px)
			int nextHeight;
			Random randomNumber = new java.util.Random();
			//Constants to determine where to start drawing the rectangles
			final int startingX = 100;
			final int startingY = 100;
			final int rectWidth = 75;

			int rectX = startingX;
			int rectY = startingY;
			// For loop to create new rectangles until the
			for(int i = 0; i < rectangles.length; i++){
				nextHeight=(randomNumber.nextInt(20));
				// Added an if statement to prevent the value from being zero so that,
				// if it is, the generator will get a new integer and add that to the zero value
				if(nextHeight==0)
					nextHeight+=(randomNumber.nextInt(20));
				// Vertical rectangles for the information that is used
				rectangles[i] = new Rectangle(rectX, (rectY+(650-(nextHeight*25))), rectWidth, nextHeight*25);
				rectX += rectWidth;
			}
			
			// An Array of Color objects which will predetermine the colors of the ten rectangles being drawn
			Color[] colorsToUse = new Color[10];
			colorsToUse[0] = Color.BLUE;
			colorsToUse[1] = Color.BLUEVIOLET;
			colorsToUse[2] = Color.RED;
			colorsToUse[3] = Color.DARKGREEN;
			colorsToUse[4] = Color.FUCHSIA;
			colorsToUse[5] = Color.YELLOW;
			colorsToUse[6] = Color.BLACK;
			colorsToUse[7] = Color.LIGHTBLUE;
			colorsToUse[8] = Color.ORANGE;
			colorsToUse[9] = Color.CHOCOLATE;

			// For loop to fill in the colors for the ten rectangles
			for(int i = 0; i < rectangles.length; i++){
				rectangles[i].setFill(colorsToUse[i]);
			}
			// Array to copy
/*			for(int i = 0; i < rectangles.length; i++){
				rectangles2[i] = rectangles[i];
			}*/
			root.getChildren().addAll(rectangles);
			primaryStage.setScene(scene);
			primaryStage.show();


			//create a timeline for moving the shapes
			timeline = new Timeline();        

			timeline.setCycleCount(Timeline.INDEFINITE);
			timeline.setAutoReverse(false);
			//one can start/pause/stop/play animation by
			//timeline.play();
			//timeline.pause();
			//timeline.stop();
			//timeline.playFromStart();
			//add the following keyframes to the timeline
			root.getChildren().add(createNavigation());
			KeyFrame moveRectangle = new KeyFrame(Duration.seconds(3));
			for(int i = 0; i < rectangles.length; i++){
				int index = 1;
				for(int j = i+1; j < rectangles.length; j++){
					if(rectangles[i].getHeight() < rectangles[index].getHeight()){
						rectangles[i].setTranslateX(rectangles[index].getX());
						rectangles[index].setTranslateX(rectangles[index+1].getX());
						timeline.getKeyFrames().add(moveRectangle);
						timeline.play();
					}
				}
			}
		}

		catch(Exception e) {
			e.printStackTrace();
		}
	}




	private VBox createNavigation(){

		//method for creating navigation panel
		//start/stop/pause/play from start buttons
		Button buttonStart = new Button("Start");
		buttonStart.setOnAction(new EventHandler<ActionEvent>() {

			public void handle(ActionEvent t) {
				//start timeline	
				timeline.play();
			}}
			);
		

		Button buttonStop = new Button("Stop");
		buttonStop.setOnAction(new EventHandler<ActionEvent>() {

			public void handle(ActionEvent t) {
				//stop timeline
				timeline.stop();
			}
		});

		Button buttonPlayFromStart = new Button("Play From Start");
		buttonPlayFromStart.setOnAction(new EventHandler<ActionEvent>() {

			public void handle(ActionEvent t) {
				//play from start
				timeline.playFromStart();
			}
		});

		Button buttonPause = new Button("Pause");
		buttonPause.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent t) {
				//pause from start
				timeline.pause();
			}
		});

		//text showing current time
		final Text currentRateText = new Text("Current time: 0 seconds" );
		currentRateText.setBoundsType(TextBoundsType.VISUAL);
		timeline.currentTimeProperty().addListener(new InvalidationListener() {
			public void invalidated(Observable ov) {
				int time = (int) timeline.getCurrentTime().toSeconds();
				currentRateText.setText("Current time: " + time + " seconds");
			}
		});
		
		//add all navigation to layout
		HBox hBox1 = new HBox(10);
		hBox1.setPadding(new Insets(0, 0, 0, 5));
		hBox1.getChildren().addAll(buttonStart, buttonPause, buttonStop, buttonPlayFromStart);
		hBox1.setAlignment(Pos.CENTER_LEFT);
		HBox hBox2 = new HBox(10);
		hBox2.setPadding(new Insets(0, 0, 0, 35));
		hBox2.getChildren().add(currentRateText);
		hBox2.setAlignment(Pos.CENTER_LEFT);
		VBox vBox = new VBox(10);
		vBox.setLayoutY(60);
		vBox.getChildren().addAll(hBox1, hBox2);
		return vBox;
	
	}
	public static void main(String[] args) {
		launch(args);
	}
	
}
