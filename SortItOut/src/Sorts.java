import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

public class Sorts extends JPanel implements ActionListener{
	
	StringBuffer str = new StringBuffer();
	
    Timer t = new Timer(500,this);
	static int x = 0;
	static int y = 0;
	static int w = 50;
	static int h = 50;

	public void paintComponent(Graphics g){
	
		super.paintComponent(g);
	    Graphics2D g2 = (Graphics2D) g;
		
		//change the origin to the center.
		g2.translate(this.getWidth()/2, this.getHeight()/2);
		//g2.scale(1, -1);
		//create the squares
		Rectangle2D square0 = new Rectangle2D.Double(x,y,w,h);
		Rectangle2D square1 = new Rectangle2D.Double(x-50,y,w,h);
		Rectangle2D square2 = new Rectangle2D.Double(x-100,y,w,h);
		Rectangle2D square3 = new Rectangle2D.Double(x+50,y,w,h);
		Rectangle2D square4 = new Rectangle2D.Double(x+100,y,w,h);
		Rectangle2D square5 = new Rectangle2D.Double(x-150,y,w,h);
		
		g2.drawString("1", 25, 25);
		g2.drawString("2", -25, 25);
		g2.drawString("3", -75, 25);
		g2.drawString("4", 75, 25);
		g2.drawString("5", 125, 25);
		g2.drawString("6", -125, 25);
		

		//  square0.add(w, h);
		//draw the squares
		
		g2.setColor(Color.darkGray);
		g2.draw(square0);
		g2.setColor(Color.BLUE);
		g2.draw(square1);
		g2.setColor(Color.CYAN);
		g2.draw(square2);
		g2.setColor(Color.YELLOW);
		g2.draw(square3);
		g2.setColor(Color.red);
		g2.draw(square4);
		g2.setColor(Color.GREEN);
		g2.draw(square5);
				
		t.start();

	}

	public void actionPerformed(ActionEvent e) {
	
		int counter = 0;
		
		for(int i =0; i<3; i++){
			y = y - 20;		
			
			repaint();
			x = x + 50;
			repaint();
			if(x == 50){
				t.stop();
				repaint();

			}
			//if(y > y*i-5){
		//	x = x*i - 5;
			//y = y/i + 5;
			//x = -x;
			
			//repaint();

			//}
		}
	

	}
	
	public static void main(String[] args) {

		Sorts sort = new Sorts();
		JFrame frame = new JFrame();
		frame.add(sort);
		frame.setVisible(true);
		frame.setSize(600,400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
	}

	
	

	
	

}
